<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<! DOCTYPE hml>

<html>

<jsp:include page="header.jsp" />

<body>
	<div class="row">
		<h1>Success!</h1>

		<a href="list" class="waves-effect waves-light btn"><i
			class="material-icons right">format_list_bulleted</i>User List</a> <a
			href="/project-justto" class="waves-effect waves-light btn"><i
			class="material-icons right">add</i>New Register</a> <a href="event-logs"
			class="waves-effect waves-light btn"><i
			class="material-icons right">linear_scale</i>Event Log</a>
	</div>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}
		/resources/js/materialize.min.js"></script>

	<sript
		src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/2.0.0-alpha.55/angular2-all-testing.umd.dev.js">
	</sript>
</body>

</html>