
<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}
		/resources/css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}
		/resources/css/style.css"
	media="screen,projection" />
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>


	<nav>
		<div class="nav-wrapper">
			<a href="#" class="brand-logo">JUSTTO</a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="/project-justto">Register</a></li>
				<li><a href="list">User List</a></li>
				<li><a href="event-logs">Event Log</a></li>
			</ul>
		</div>
	</nav>