<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<! DOCTYPE hml>

<html>

<jsp:include page="header.jsp" />

<body>

	<div class="row">
		<br>
		<br>
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>Telephone</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="temp" items="${users}">
					<tr>
						<td>${temp.id}</td>
						<td>${temp.name}</td>
						<td>${temp.email}</td>
						<td>${temp.telephone}</td>
						<td><a href="edit?user_id=${temp.id}"> <i
								class="tiny material-icons color-blue">edit</i>
						</a> | <a href="remove?user_id=${temp.id}"> <i
								class="tiny material-icons color-red">delete</i>
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}
		/resources/js/materialize.min.js"></script>

	<sript
		src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/2.0.0-alpha.55/angular2-all-testing.umd.dev.js">
	</sript>
</body>

<script>
	
</script>
</html>