<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<! DOCTYPE hml>

<html>

<jsp:include page="header.jsp" />

<body>

	<div class="row">
		<h5>Event Log</h5>

		<ul class="collection">
			<c:forEach var="temp" items="${logs}">
				<c:choose>
					<c:when test="${temp.type == '1'.charAt(0)}">
						<c:set var="color_class" scope="session" value="log-green" />
						<c:set var="icon" scope="session" value="add" />
					</c:when>
					<c:when test="${temp.type == '2'.charAt(0)}">
						<c:set var="color_class" scope="session" value="log-orange" />
						<c:set var="icon" scope="session" value="edit" />
					</c:when>
					<c:otherwise>
						<c:set var="color_class" scope="session" value="log-red" />
						<c:set var="icon" scope="session" value="remove" />
					</c:otherwise>
				</c:choose>
				<li class="collection-item"><i
					class="tiny material-icons ${color_class}">${icon}</i> <span>${temp.log}
						<fmt:formatDate type="time" value="${temp.dateTimeIn}"></fmt:formatDate>
				</span></li>
			</c:forEach>
		</ul>
	</div>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}
		/resources/js/materialize.min.js"></script>

	<sript
		src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/2.0.0-alpha.55/angular2-all-testing.umd.dev.js">
	</sript>
</body>
</html>