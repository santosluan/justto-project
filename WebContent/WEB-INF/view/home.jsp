<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<! DOCTYPE hml>

<html ng-app>

<jsp:include page="header.jsp" />

<div class="row">
	<br>
	<br>
	<form:form class="col s12" ng-controller="justto"
		action="${action == 'edit' ? 'updateUser' : 'createUser'}?user_id=${param['user_id']}"
		modelAttribute="user">
		<div class="row">
			<div class="input-field col s12">
				<form:input id="first_name" type="text" class="validate" path="name" />
				<label for="first_name">First Name (*)</label>
				<form:errors path="name" cssClass="error" />
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<form:input id="telephone" type="text" class="validate"
					path="telephone" />
				<label for="telephone">Telephone</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<!--           <input id="email" type="email" class="validate"> -->
				<form:input id="email" type="text" class="validate" path="email" />
				<label for="email">Email (*)</label>
				<form:errors path="email" cssClass="error" />
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<form:input id="password" type="password" class="validate"
					path="password" />
				<label for="password">Password (*)</label>
				<form:errors path="password" cssClass="error" />
			</div>
		</div>

		<button class="btn waves-effect waves-light" type="submit"
			name="action">
			<i class="material-icons right">send</i>${action == "edit" ? "Update" : "Submit"}
		</button>
	</form:form>
</div>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript"
	src="${pageContext.request.contextPath}
		/resources/js/materialize.min.js"></script>

<sript
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/2.0.0-alpha.55/angular2-all-testing.umd.dev.js">
</sript>

</body>

</html>