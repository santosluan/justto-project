package com.justto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateSession {

	SessionFactory factory; 
	Session session;

	public HibernateSession() {
		// create session factory			
		factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(User.class)
				.buildSessionFactory();		    		    		       

		// create session		   
		session = factory.getCurrentSession();			
	}

	public void insertUser(User user) {

		try {
			// user the session object to save Java object
			System.out.println("Creating a new user object");		    	

			// start a transaction
			session.beginTransaction();

			// save the user object
			System.out.println("Saving the user...");
			session.save(user);

			// commit transaction
			session.getTransaction().commit();

			// find out the student's id: primary key
			System.out.println("Saved student. Generated id: " + user.getId());

			System.out.println("Done!");
		} finally {
			session.close();
		}		    
	}   

	public User retrieveUser(Long id) {

		try {
			// get a new session and start transaction			
			session = factory.getCurrentSession();
			session.beginTransaction();

			// retrieve student based on the id: primary key
			System.out.println("\nGetting user with id" + id);

			User user = session.get(User.class, id);

			System.out.println("Get complete: " + user);

			// commit the transaction
			session.getTransaction().commit();

			System.out.println("Done!");

			return user;

		} finally {
			session.close();
		}
	}

	public List<User> retrieveAll() {

		try {
			// get a new session and start transaction			
			session = factory.getCurrentSession();
			session.beginTransaction();

			List<User> user =
					session
					.createQuery("from User")
					.getResultList();

			System.out.println(user.toString());

			// commit the transaction
			session.getTransaction().commit();

			System.out.println("Done!");

			return user;
		} finally {
			session.close();
		}
	}			

	public boolean updateUser(Long userId, User user) {

		try {
			// get a new session and start transaction			
			session = factory.getCurrentSession();
			session.beginTransaction();		

			// get user
			User tmpUser = session.get(User.class, userId);				

			tmpUser.setName(user.getName());
			tmpUser.setTelephone(user.getTelephone());
			tmpUser.setEmail(user.getEmail());
			tmpUser.setPassword(user.getPassword());

			System.out.println(tmpUser);

			// updatting user
			session.getTransaction().commit();			

			System.out.println("Done!");

			return true;
		} finally {
			session.close();
		}
	}				

	public User removeUser(Long id) {

		try {
			// get a new session and start transaction			
			session = factory.getCurrentSession();
			session.beginTransaction();		

			// get user
			User user = session.get(User.class, id);

			// operation
			session.delete(user);

			// deleting user
			System.out.println("Deleting user...");
			session.getTransaction().commit();			

			System.out.println("Done!");	

			return user;
		} finally {
			session.close();
		}
	}						

}
