package com.justto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="log")
public class Log {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)		
	@Column(name="id")
	private Long id;
	
	@Column(name="log")
	private String log;
	
	@Column(name="type")
	private char type;
	
	@Column(name="date_time_in")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateTimeIn;
	
	public Log() {
		
	    Date now = new Date();
		dateTimeIn = now;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public Date getDateTimeIn() {
		return dateTimeIn;
	}

	public void setDateTimeIn(Date dateTimeIn) {
		this.dateTimeIn = dateTimeIn;
	}
	
}
