package com.justto;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String homePage(Model userModel) {

		User user = new User();
		userModel.addAttribute("user", user);

		return "home";
	}

	@RequestMapping("/createUser")
	public String createUser(
			@Valid @ModelAttribute("user") User user,
			BindingResult bindingResult) {

		if(bindingResult.hasErrors())
			return "home";

		System.out.println(user.getEmail());

		// getting hibernate session
		HibernateSession hb = new HibernateSession();

		// getting datetime
		Date now = new Date();

		// setting datetime on user object
		user.setDatetime(now);

		// create user
		hb.insertUser(user);

		// generating log
		LogOperation lo = new LogOperation();	    
		Log log = new Log();

		log.setLog("User " + user.getName() + " criated at: ");
		log.setType('1');

		lo.insertLog(log);

		return "confirmation";
	}

	@RequestMapping("/updateUser")
	public String updateUser(
			@Valid @ModelAttribute("user") User user,
			BindingResult bindingResult,
			@RequestParam("user_id") int userId,
			Model model) {

		if(bindingResult.hasErrors()) {			
			model.addAttribute("action", "edit");
			return "home";
		}

		// getting hibernate session
		HibernateSession hb = new HibernateSession();	

		System.out.println("USER ID OF MODEL ==== " + userId);

		hb.updateUser(Long.valueOf(userId), user);

		// generating log
		LogOperation lo = new LogOperation();	    
		Log log = new Log();

		log.setLog("User " + user.getName() + " updated at: ");
		log.setType('2');

		lo.insertLog(log);

		return "confirmation";
	}

	@RequestMapping("/list")
	public String listUsers(Model model) {

		// getting hibernate session
		HibernateSession hb = new HibernateSession();

		List<User> users = hb.retrieveAll();

		model.addAttribute("users", users);

		return "list";

	}

	@RequestMapping("/edit")
	public String editUser(Model model,
			@RequestParam("user_id") int userId) {

		System.out.println("ID USER === " + userId);


		// getting hibernate session
		HibernateSession hb = new HibernateSession();

		User user = hb.retrieveUser(Long.valueOf(userId));

		model.addAttribute("user", user);
		model.addAttribute("action", "edit");

		return "home";

	}

	@RequestMapping("/remove")
	public String removeUSer(@RequestParam("user_id") int userId) {

		System.out.println("ID TO REMOVE === " + userId);

		// getting hibernate session
		HibernateSession hb = new HibernateSession();

		User user_remove = hb.removeUser(Long.valueOf(userId));

		// generating log
		LogOperation lo = new LogOperation();	    
		Log log = new Log();

		log.setLog("User " + user_remove.getName() + " removed at: ");
		log.setType('3');

		lo.insertLog(log);

		return "confirmation";
	}

	@RequestMapping("/event-logs")
	public String eventLogs(Model model) {

		// getting hibernate session
		LogOperation lo = new LogOperation();

		List<Log> logs = lo.retrieveAll();

		model.addAttribute("logs", logs);

		return "event-log";
	}
}
