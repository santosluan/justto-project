package com.justto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="user")
public class User {
	
	@Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	@NotNull(message="is required")
	@Size(min=3, message="Minimum size: 3")	
	private String name;
	
	@Column(name="telephone")
	private String telephone;
	
	@Column(name="email")
	@NotNull(message="is required")
	@Size(min=6, message="Minimum size: 6")		
	private String email;
	
	@Column(name="password")
	@NotNull(message="is required")
	@Size(min=6, message="Minimum size: 6")		
	private String password;
	
	@Column(name="date_time_in")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateTimeIn;

	public Long getId() {
		return id;
	}
	
	public void setId(Long Id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getDatetime() {
		return dateTimeIn;
	}

	public void setDatetime(Date datetime) {
		this.dateTimeIn = datetime;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", telephone=" + telephone + ", email=" + email + ", password="
				+ password + "]";
	}
	
	
}
