package com.justto;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class LogOperation {

	SessionFactory factory; 
	Session session;

	public LogOperation() {
		// create session factory			
		factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Log.class)
				.buildSessionFactory();		    		    		       
	    
	    // create session		   
	    session = factory.getCurrentSession();	
	    
	}

	public void insertLog(Log log) {
	    
	    try {
	    	// user the session object to save Java object
	    	System.out.println("Creating a new user object");		    	
	    	   	
	    	// start a transaction
		    session.beginTransaction();
	    	
	    	// save the user object
		    System.out.println("Saving log...");
		    session.save(log);
	    	
	    	// commit transaction
		    session.getTransaction().commit();
		    
		    // find out the student's id: primary key
		    System.out.println("Saved student. Generated id: " + log.getId());
		    
		    System.out.println("Done!");
	    } finally {
	    	session.close();
	    }		    
	}   
	
	public List<Log> retrieveAll() {
		
		try {
			// get a new session and start transaction			
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			List<Log> log =
					session
					.createQuery("from Log ORDER BY Id DESC")
					.getResultList();
						
			// commit the transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
			
			return log;
		} finally {
			session.close();
		}
	}	
}
